const express = require("express");
const router = express.Router();
const checkClient = require("../middleware/clientMiddleware");
const upload = require("../controllers/fileUploadController");

router.post("/", checkClient, upload.single("profile"), (req, res) => {
  res.json({
    success: true,
    profile_url: `http://localhost:9000/profile/${req.file.filename}`,
  });
});

module.exports = router;
