const express = require("express");
const { signUp, Login, logout } = require("../controllers/authController");

const router = express.Router();

router.post("/signup", signUp);

router.post("/login", Login);

router.get("/logout", logout);

module.exports = router;
