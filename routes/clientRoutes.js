const express = require("express");
const router = express.Router();
const {
  getAllClient,
  getClientById,
  createClient,
  updateClientFully,
  updateClient,
  deleteClient,
} = require("../controllers/clientController");
const checkClient = require("../middleware/clientMiddleware");

router.get("/", checkClient, getAllClient);

router.get("/:id", checkClient, getClientById);

router.post("/", checkClient, createClient);

router.put("/:id", checkClient, updateClientFully);

router.patch("/:id", checkClient, updateClient);

router.delete("/:id", checkClient, deleteClient);

module.exports = router;
