const jwt = require("jsonwebtoken");

const loggedInUser = (req, res) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    let user = jwt.verify(token, "Intellia tech");
    return user.id;
  } catch (error) {
    return res.status(401).json({
      message: "User Unauthorised",
    });
  }
};

module.exports = loggedInUser;
