const express = require("express");
const mongoose = require("mongoose");
const url = "mongodb://127.0.0.1:27017/hr";
const cookieParser = require("cookie-parser");
const { requireAuth, checkUser } = require("./middleware/authMiddleware");

const app = express();

//middleware
app.use(express.json());
app.use(cookieParser());

mongoose.connect(url, { useNewUrlParser: true });

const con = mongoose.connection;

const clientSchema = require("./routes/clientRoutes");
app.use("/clients", clientSchema);

const authRouters = require("./routes/authRoutes");
app.use("/users", authRouters);

const fileRouter = require("./routes/fileUploadRoutes");
app.use("/uploads", fileRouter);

con.on("open", () => {
  console.log("DB Connected !!!");
});

app.listen(9000, () => {
  console.log("Server Started");
});
