const User = require("../models/User");
const jwt = require("jsonwebtoken");

//handle Error
const handleErrors = (err) => {
  let errors = { email: "", password: "" };

  //Incorrect Email
  if (err.message === "Invalid Email") {
    errors.email = "Incorrect Email";
  }

  //Incorrect Password
  if (err.message === "Invalid password") {
    errors.password = "Incorrect Password";
  }

  //duplicate Errors
  if (err.code == 11000) {
    errors.email = "Email already exists";
    return errors;
  }

  //validation Errors
  if (err.message.includes("user validation failed")) {
    Object.values(err.errors).forEach(({ properties }) => {
      errors[properties.path] = properties.message;
    });
  }

  return errors;
};

//create token
const maxAge = 60 * 60;
const createToken = (id) => {
  return jwt.sign({ id }, "Intellia tech", {
    expiresIn: maxAge,
  });
};

const signUp = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.create({ email, password });
    const token = createToken(user._id);
    // res.cookie("jwt", token, { httpOnly: true, maxAge: maxAge * 1000 });
    res
      .status(201)
      .json({ msg: "User registered successfully!!!", user: user._id });
  } catch (err) {
    const errors = handleErrors(err);
    res.status(400).json({ errors });
  }
};

const Login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.login(email, password);
    const token = createToken(user._id);
    // res.cookie("jwt", token, { httpOnly: true, maxAge: maxAge * 1000 });
    res
      .status(200)
      .json({
        status: "success",
        msg: "Login successful",
        user: user._id,
        token: token,
      });
  } catch (err) {
    const errors = handleErrors(err);
    res.status(400).json({ errors });
  }
};

const logout = (req, res) => {
  // res.cookie("jwt", "", { maxAge: 1 });
  res.status(200).json({ status: "success", msg: "User logout" });
};

module.exports = { signUp, Login, logout };
