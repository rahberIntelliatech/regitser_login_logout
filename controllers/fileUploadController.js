const multer = require("multer");
const path = require("path");
const loggedInUser = require("../utils/loggedInUserToken");

const storage = multer.diskStorage({
  destination: "./uploads/images",
  filename: (req, file, cb) => {
    return cb(
      null,
      `${loggedInUser(req)}_${file.fieldname}_${path.extname(
        file.originalname
      )}`
    );
  },
});
const upload = multer({
  storage: storage,
});

module.exports = upload;
