const mongoose = require("mongoose");

const { isEmail } = require("validator");

const clientSchema = new mongoose.Schema({
  company_name: {
    type: String,
    required: [true, "Please enter a company name"],
  },
  client_id: {
    type: Number,
    required: [true, "Please enter a client id"],
  },
  contact_person: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: [true, "Please enter an email"],
    unique: true,
    lowercase: true,
    validate: [isEmail, "Please enter a valid email"],
  },
  phone: {
    type: Number,
    unique: true,
    required: [true, "Please enter a mobile number"],
  },
  status: {
    type: Boolean,
    required: true,
    default: true,
  },
});

module.exports = mongoose.model("Client", clientSchema);
